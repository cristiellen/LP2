package jogodavelha;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Interface2 {

    private JPanel linha1;
    private JPanel linha2;
    private JPanel linha3;
    private JTextField um;
    private JTextField dois;
    private JTextField tres;
    private JTextField quatro;
    private JTextField cinco;
    private JTextField seis;
    private JTextField sete;
    private JTextField oito;
    private JTextField nove;
    private JFrame janela2;
    private boolean jogada;

    public void construir(String nome1, String nome2, boolean j) {

        this.jogada = j;
        janela2 = new JFrame(nome1 + " X " + nome2);
        janela2.setBounds(750, 300, 350, 360);
        janela2.getContentPane().setLayout(new GridLayout(3, 3));

        linha1 = new JPanel();
        linha2 = new JPanel();
        linha3 = new JPanel();

        um = new JTextField();
        dois = new JTextField();
        tres = new JTextField();
        quatro = new JTextField();
        cinco = new JTextField();
        seis = new JTextField();
        sete = new JTextField();
        oito = new JTextField();
        nove = new JTextField();

        um.setPreferredSize(new Dimension(100, 100));
        dois.setPreferredSize(new Dimension(100, 100));
        tres.setPreferredSize(new Dimension(100, 100));
        quatro.setPreferredSize(new Dimension(100, 100));
        cinco.setPreferredSize(new Dimension(100, 100));
        seis.setPreferredSize(new Dimension(100, 100));
        sete.setPreferredSize(new Dimension(100, 100));
        oito.setPreferredSize(new Dimension(100, 100));
        nove.setPreferredSize(new Dimension(100, 100));

        linha1.add(um);
        linha1.add(dois);
        linha1.add(tres);
        linha2.add(quatro);
        linha2.add(cinco);
        linha2.add(seis);
        linha3.add(sete);
        linha3.add(oito);
        linha3.add(nove);

        um.setEditable(false);
        dois.setEditable(false);
        tres.setEditable(false);
        quatro.setEditable(false);
        cinco.setEditable(false);
        seis.setEditable(false);
        sete.setEditable(false);
        oito.setEditable(false);
        nove.setEditable(false);

        um.setBackground(Color.BLACK);
        dois.setBackground(Color.BLACK);
        tres.setBackground(Color.BLACK);
        quatro.setBackground(Color.BLACK);
        cinco.setBackground(Color.BLACK);
        seis.setBackground(Color.BLACK);
        sete.setBackground(Color.BLACK);
        oito.setBackground(Color.BLACK);
        nove.setBackground(Color.BLACK);

        janela2.getContentPane().add(linha1);
        janela2.getContentPane().add(linha2);
        janela2.getContentPane().add(linha3);

        janela2.setDefaultCloseOperation(EXIT_ON_CLOSE);
        janela2.setVisible(true);

        acoesBotoes();

    }

    public void jogada(JTextField num) {
        if (jogada) {
            num.setText("X");
            jogada = false;
        } else {
            num.setText("O");
            jogada = true;
        }
        num.setEnabled(false);
    }

    public void acoesBotoes() {
        um.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_NUMPAD1) {
                    jogada(um);
                }
            }
        });

        /*um.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogada(um);
            }
        });*/
        dois.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_NUMPAD2) {
                    jogada(dois);
                }
            }
        });
        /* dois.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogada(dois);
            }
        });*/

        tres.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_NUMPAD3) {
                    jogada(tres);
                }
            }
        });
        /*tres.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogada(tres);
            }
        });*/

        quatro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_NUMPAD4) {
                    jogada(quatro);
                }
            }
        });
        /*quatro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogada(quatro);
            }
        });*/

        cinco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_NUMPAD5) {
                    jogada(cinco);
                }
            }
        });
        /*cinco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogada(cinco);
            }
        });*/

        seis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_NUMPAD6) {
                    jogada(seis);
                }
            }
        });
        /*seis.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogada(seis);
            }
        });*/

        sete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_NUMPAD7) {
                    jogada(sete);
                }
            }
        });
        /*quatro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogada(quatro);
            }
        });*/

        oito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_NUMPAD8) {
                    jogada(oito);
                }
            }
        });
        /*oito.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogada(oito);
            }
        });*/

        nove.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_NUMPAD9) {
                    jogada(nove);
                }
            }
        });
        /*quatro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jogada(quatro);
            }
        });*/

    }

}
