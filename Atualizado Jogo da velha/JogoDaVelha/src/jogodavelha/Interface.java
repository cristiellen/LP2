package jogodavelha;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Interface {

    private JFrame janela;
    private JPanel painelGeral;
    private JPanel painelJogador1;
    private JPanel painelJogador2;
    private JPanel painelComeca;
    private JPanel painelJogar;
    private JTextField textoNome1;
    private JTextField textoNome2;
    private JLabel nomeLabel1;
    private JLabel nomeLabel2;
    private JLabel quemComeca;
    private JRadioButton radio1;
    private JRadioButton radio2;
    private ButtonGroup group;
    private JButton botao;
    private String jogador1;
    private String jogador2;
    private boolean vez;

    public void construir() {
        janela = new JFrame("");
        janela.setBounds(750, 300, 700, 350);
        janela.getContentPane().setLayout(new FlowLayout());
        painelGeral = new JPanel(new GridLayout(6, 1));
        painelJogador1 = new JPanel();
        painelJogador2 = new JPanel();
        painelComeca = new JPanel();
        painelJogar = new JPanel();

        radio1 = new JRadioButton("Jogador1");
        radio2 = new JRadioButton("Jogador2");
        nomeLabel1 = new JLabel("Jogador 1: ");
        nomeLabel2 = new JLabel("Jogador 2: ");
        quemComeca = new JLabel("Quem começa? ");
        textoNome1 = new JTextField();
        textoNome2 = new JTextField();
        botao = new JButton("Jogar");

        textoNome1.setPreferredSize(new Dimension(250, 30));
        textoNome2.setPreferredSize(new Dimension(250, 30));

        painelJogar.add(botao);

        painelJogador1.add(nomeLabel1);
        painelJogador1.add(textoNome1);
        painelJogador2.add(nomeLabel2);
        painelJogador2.add(textoNome2);

        group = new ButtonGroup();

        group.add(radio1);
        group.add(radio2);

        painelGeral.add(painelJogador1);
        painelGeral.add(painelJogador2);
        painelComeca.add(quemComeca);
        painelGeral.add(painelComeca);
        painelGeral.add(radio1);
        painelGeral.add(radio2);
        painelGeral.add(painelJogar);

        janela.getContentPane().add(painelGeral);

        janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
        acoesDosBotoes();
        janela.setVisible(true);

    }

    public void acoesDosBotoes() 
    {

        botao.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                boolean jogador;

                if (radio1.isSelected()) 
                {
                    jogador = true;
                } else 
                {
                    jogador = false;
                }

                Interface2 j = new Interface2();
                j.construir(textoNome1.getText(), textoNome2.getText(), jogador);
                janela.dispose();

            }
        });

       /* botao.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {

            }

            @Override
            public void mousePressed(MouseEvent me) {

            }

            @Override
            public void mouseReleased(MouseEvent me) {

            }

            @Override
            public void mouseEntered(MouseEvent me) {

            }

            @Override
            public void mouseExited(MouseEvent me) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });*/

    }

}
